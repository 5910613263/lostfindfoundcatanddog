Feature: Manage post
  In order to [goal]
  [stakeholder]
  wants [behaviour]
  Scenario: Creat new post
    Given kukkik is logged in
    And I am on the new post page
    When I choose post_catagory_lost
    And I fill in post[title] with น้องมูมู่หาย
    And I fill in post[content] with น้องแมวลายส้ม ตัวเล็กๆ หายบริเวณฟิวเจอร์พาร์ครังสิต
    And I attach a file
    And I press Create Post
    Then I should see the text Post was successfully created.

  Scenario: Edit post
    Given kukkik is logged in
    And I have the post
    And I am on the edit post page
    When I choose post_catagory_lost
    And I fill in post[title] with น้องมีมี่หาย
    And I fill in post[content] with น้องแมวลายส้ม ตัวเล็กๆ หายบริเวณฟิวเจอร์พาร์ครังสิต
    And I attach a file
    And I press Update Post
    Then I should see the text Post was successfully updated.

  Scenario: Delete post
    Given kukkik is logged in
    And I have the post
    When I cilck icon delete
    Then I should see the text Post was successfully destroyed.

