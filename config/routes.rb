Rails.application.routes.draw do
  get 'about/about'
  get 'lost/lost'

  resources :posts
  root 'home#index'
  
  resources :users
  resources :sessions, only: [:new, :create, :destroy]
  resources :posts, only: [:new, :create, :destroy]

  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  # get 'posts', to: 'posts#new', as: 'posts'
  get 'mypost', to: 'posts#mypost', as: 'mypost'
  get '/search',to: 'posts#search', as: 'search_page'
  get 'posts/create', to: 'posts#create', as: 'create'
  get 'posts/show', to: 'posts#show', as: 'show'
  get 'posts/index', to: 'posts#index', as: 'index'
  get '/lost', to: 'posts#lost', as: 'lost'
  get '/find', to: 'posts#find', as: 'find'
   get '/found', to: 'posts#found', as: 'found'
  get '/about', to: 'about#about', as: 'about'
end